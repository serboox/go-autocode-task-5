package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	val1, err := myStrToInt("1")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	fmt.Printf("%d\n", val1)
}

func myStrToInt(s string) (int, error) {
	return strconv.Atoi(s)
}
