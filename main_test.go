package main

import (
	"testing"
)

type squareTestCase struct {
	value    string
	expected int
}

var squareTestCases = []squareTestCase{
	{"1", 1},
	{"2", 2},
	{"3", 3},
	{"4", 4},
	{"5", 5},
}

func TestStrToInt(t *testing.T) {
	for _, testCase := range squareTestCases {
		val1, err := myStrToInt(testCase.value)
		if err != nil {
			t.Fatal(err)
		}
		if val1 != testCase.expected {
			t.Fatalf(
				"For %#v expected %d got %d",
				testCase,
				testCase.expected,
				val1,
			)
		}
	}
}
